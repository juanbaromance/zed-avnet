#!/bin/bash

KERNEL="-kernel uImage"
#RAM_DISK="--initrd uramdisk.image.gz"
MACHINE="-M xilinx-zynq-a9 -dtb system.dtb"
NETWORKING="-net nic -net user,hostfwd=tcp:127.0.0.1:10022-10.0.2.15:22"
CONSOLE="-serial /dev/null -serial mon:stdio -display none"

~/bin/qemu-system-aarch64 ${MACHINE} ${CONSOLE} ${NETWORKING} ${KERNEL} ${RAM_DISK}
# On your local host :: ssh localhost -p 10022 -l root
