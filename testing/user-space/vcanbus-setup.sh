# !/bin/bash

device_deployment()  {

  device=$1
  printf "\n# `date` : Let's build socketCAN( ${device} )\n"
  if [ -z "`lsmod | grep vcan`" ]; then
    printf "# Installing socketCAN VirtualCAN driver\n"
    insmod vcan.ko;
  fi;

  if [ -z "`cat /proc/net/dev | grep ${device}`" ]; then
    printf "# Creating VirtualCAN device : ${device}\n"
    ip link add dev ${device} type vcan
    ip -h -d -c link show ${device}
  fi;

  if [ -n "`ip -h -d -c link show ${device} | grep DOWN`" ]; then
    printf "# Bringing Up SocketCAN : ${device} link\n"
    ip link set up ${device}
  else
    printf "# Network link( ${device} ) is already up\n"
  fi;

  ip -h -d -c link show ${device}

}

enroute_deployment()
{

  # Now route both devices 
  export PATH=/opt/shared/can-utils/:$PATH;
  vBusRules=(
    '-s vcan1 -d vcan0' 
    '-s vcan0 -d vcan1'
  )
  
  printf "\n\n# `date` : Let's enroute VCAN devices $1 $2\n"
  
  count=0
  while [ "x${vBusRules[count]}" != "x" ]

  do
    
    deleter="cangw -D ${vBusRules[count]} -e"
    while [ true ]
    do
      `${deleter}>&/dev/null`
      if [ $? != 0 ]; then 
      break
      fi
      printf "# \"${vBusRules[count]}\" route cleanup : success\n"
    done
    
    create="`echo ${deleter} | sed -e "s/-D/-A/g"`"
    `${create}`
    count=$(( $count + 1 ))
  done

    cangw -L
    printf "\n";

}

main()
{

    vBusSpecification="vcan0 vcan1"
    for var in ${vBusSpecification}
    do
    device_deployment ${var}
    done

    enroute_deployment ${vBusSpecification}
}

main