#!/bin/bash
device="tap1"

sudo tunctl -t ${device} -u `whoami`
sudo ifconfig ${device} 10.0.2.16

source /opt/development/zynq/petalinux-sdk/2019.2/settings.sh
petalinux-boot --qemu --kernel --qemu-args "-net nic -net tap,ifname=tap1"
