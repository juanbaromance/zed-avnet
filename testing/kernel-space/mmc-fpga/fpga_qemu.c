#include "fpga_qemu.h"
#include "fpga_udp.h"
#include "fpga_drv.h"

#include <linux/mutex.h>
#include <linux/timer.h>
#include <linux/kthread.h>

#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/jiffies.h>
#include <linux/delay.h>

static void sync_signature( void );
static int  registers_sampler( void *context );


struct WatchdogDevice
{
    struct WatchdogDevice* ( *init )( void );
    void ( *destroy  )( void );
    void ( *callback )( struct timer_list* context );

    GenericObserver_t observer;

    char *name;
    struct timer_list ktimer;
    size_t deadline, elapsed, sampling;
    union  {
        __u8 all;
        struct {
            __UINT8_TYPE__ quit:1;
            __UINT8_TYPE__ trace_level:3;
        }bit;
    }csr;
};

static void watchdog_callback ( struct timer_list* context );
static struct WatchdogDevice* watchdog_init( void );
static void watchdog_destroy( void );

struct QEMUfpga
{
    const char *name;
	void ( *sync_signature )( void  );
    int  ( *image_sampler  )( void* );

    struct mutex * chipset_locker;
    void *base_address;
    struct fpga_regs *registers, image;
    struct task_struct *sampler_task;

    enum Numerology {
        A3633    = 3633,
        Version  = 0x02,
        Revision = 0x67,
        FPGAVersion = 0x02,
        FPGARevision = 0x00,
        FPGAPatch = 0x01,
        FPGASignature = 
            ( ( FPGAVersion  & 0xf ) << 12 ) | 
            ( ( FPGARevision & 0xf ) << 8  ) | 
            ( FPGAPatch & 0xff ),
        _1000msec = 1000
    };

    struct WatchdogDevice *watchdog;
    struct iUDPPort *udp_port;
};

typedef struct QEMUfpga QEMUfpga;

static struct QEMUfpga device = {
    .name = "QEMUfpga",
	.sync_signature = sync_signature,
    .image_sampler  = registers_sampler,
};

inline u8 ascii2hex(char c){ return ( c <='9' ) ? ( c -'0' ) : ( c - 'A' +10 ); }

void QemuFPGAStartUp( void *base_address )
{

    device.chipset_locker =  (struct mutex*)kmalloc( sizeof( struct mutex), GFP_KERNEL);
    mutex_init( device.chipset_locker );

	device.base_address = base_address;
	device.registers = ( struct fpga_regs * )( device.base_address );
	device.sync_signature();
    device.sampler_task = kthread_run( device.image_sampler,(void*)( & device ), device.name);
    device.watchdog = watchdog_init();
    device.udp_port = UDPConnection( "QEMUfpga", base_address );
    memcpy( & device.image, device.registers, sizeof( struct fpga_regs ) );
}

void QemuFPGAFinish( void )
{
    if( device.sampler_task )
        kthread_stop( device.sampler_task );
    device.watchdog->destroy();
    device.udp_port->destroy();
}

/*
HW    : PBA[A3633-02-C]
FPGA  : DNA[01303a76 a37d071e] HDL[V02R00.01]
*/
static void sync_signature( void )
{
    u64 dna = 0x01303a76a37d071e;
    ((u64*)( device.registers->version.dna ))[0] = le64_to_cpu(dna);
    device.registers->version.code = le16_to_cpu(A3633);
    device.registers->version.ver  = Version;
    device.registers->version.rev  = Revision;
    device.registers->version.firmware = le16_to_cpu(FPGASignature);
}

static int registers_sampler( void *context )
{
    QEMUfpga *device = (QEMUfpga*)context;
    size_t i = 0, elapsed = 0;
    const size_t msec_sampling = 100;
    const size_t so_chipset = sizeof( struct fpga_regs );
    printk( "%s.%s( sampling spec :%4d msec )\n", device->name, __FUNCTION__ , msec_sampling );
    while ( ! kthread_should_stop() ) 
    {
        mutex_lock( device->chipset_locker );
        elapsed  += msec_sampling;
        msleep( msec_sampling );
        i++ ;
        {
            int change;
            if( ( change = memcmp( & device->image, device->registers, so_chipset ) ) != 0 )
            {
                printk( "%s.%s : change(%d) sampled on chipset(%d)\n", device->name, __FUNCTION__ , change, sizeof(device->image) );
                device->udp_port->xmitt( ( void* )device->registers );
                device->image = *device->registers; 
            }
        }
        mutex_unlock( device->chipset_locker );
    }

    kfree( device->chipset_locker );
    printk( "%s.%s : quits now\n", device->name, __FUNCTION__ );
    return 0;
}

void sync_chipset_image(uint32_t operation, uint32_t reg_val )
{
    mutex_lock( device.chipset_locker );
    uint32_t i;
    for( i = 0; operation; i++ )
    {
        if( test_bit( i,  KERNEL_REF( operation ) ) )
        {
            switch( i )
            {
            case QEMUGPiUpdate:
                device.registers->gpi = device.image.gpi = reg_val & 0xff;
                break;
            }
        }
        clear_bit( i, KERNEL_REF(operation) );
    }
    mutex_unlock( device.chipset_locker );
}


static void watchdog_callback( struct timer_list* context )
{
    struct WatchdogDevice *wdg  = device.watchdog;
    if( wdg->csr.bit.quit )
        return;
    wdg->elapsed += wdg->sampling;
    if( wdg->csr.bit.trace_level > 2 )
        printk( "%s.%s : %d versus %d\n", device.name, __FUNCTION__ , wdg->elapsed, wdg->deadline );

    if(  wdg->elapsed < wdg->deadline )
    {
        GenericObserver_t *observer = & wdg->observer;
        if( observer->symbol )
        {
            int ret_val;
            if( ( ret_val = observer->symbol( observer->context ) ) < 0 )
            {
                if( ret_val == -2 )
                {
                    printk( "%s.%s: grounded by observer module (%d)\n", device.name, __FUNCTION__ , ret_val );
                    wdg->csr.bit.quit = true;
                    observer->symbol = NULL;
                    return;
                }
                else
                    wdg->elapsed = 0;
            }
        }
        mod_timer( & wdg->ktimer, jiffies + msecs_to_jiffies(wdg->sampling));
    }
    else
    {
        printk( "%s.%s(%4d msec):exhausted\n", device.name, __FUNCTION__ , wdg->elapsed);
        wdg->elapsed = 0;
    }
    return;
}

static void watchdog_destroy( void )
{
    struct WatchdogDevice *wdg  = device.watchdog;
    int ret = del_timer( & wdg->ktimer );
    if( ret )
    {
        printk("%s.%s : watchdog is still in use...\n", wdg->name, __FUNCTION__ );
        wdg->csr.bit.quit = true;
        mod_timer( & wdg->ktimer, jiffies + msecs_to_jiffies(10));
        msleep( 30 );
        del_timer( & wdg->ktimer );
    }
    kfree( wdg );
    return;
}

static struct WatchdogDevice *watchdog_init()
{
    struct WatchdogDevice *wdg =( struct WatchdogDevice *)kmalloc( sizeof( struct WatchdogDevice), GFP_KERNEL );
    wdg->elapsed = 0;
    wdg->callback = watchdog_callback;
    wdg->destroy = watchdog_destroy;
    wdg->csr.bit.trace_level = 0;
    wdg->csr.bit.quit = false;

    wdg->sampling = _1000msec;
    wdg->deadline = _1000msec << 2;
    wdg->name = "QEMUfpga.Watchdog";
    wdg->observer.symbol = wdg->observer.context = NULL;

    timer_setup( & wdg->ktimer, wdg->callback, 0);
    mod_timer( & wdg->ktimer, jiffies + msecs_to_jiffies(wdg->sampling));//1000 milli-seocnd in the future
    return wdg;
}


void watchdog_startup( const TickObserver_t *o )
{
    struct WatchdogDevice *wdg = device.watchdog;
    wdg->elapsed = 0;
    wdg->csr.bit.trace_level = 0;
    wdg->csr.bit.quit = false;

    wdg->sampling = o->interval;
    wdg->deadline = o->deadline;
    wdg->name = "QEMUfpga.Tick";
    wdg->observer.symbol = o->feedback.symbol;
    wdg->observer.context = o->feedback.context;
    mod_timer( & wdg->ktimer, jiffies + msecs_to_jiffies(wdg->sampling));
}

