#ifndef FPGAUDPInclude
#define FPGAUDPInclude

#include <linux/types.h>

struct iUDPPort
{
    void ( *destroy  )( void );
    void ( *xmitt    )( void* payload );
    char *name;
};

struct iUDPPort* UDPConnection(const char *connection_name , void *chipset_image );

#endif
