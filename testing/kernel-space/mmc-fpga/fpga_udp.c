#include "fpga_udp.h"
#include "fpga_qemu.h"
#include "QemuFPGA.iface.h"
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/init.h>
#include <linux/inetdevice.h>
#include <linux/ip.h>
#include <linux/in.h>
#include <asm-generic/types.h>
#include <linux/netlink.h>
#include <linux/wait.h>
#include <linux/ctype.h>
#include <net/sock.h>
#include <net/tcp.h>
#include <net/inet_connection_sock.h>
#include <net/request_sock.h>


typedef struct iUDPPort iUDPPort;
typedef struct socket kSocket;

struct UDPParameters {
    uint32_t port, ipaddr;
};
typedef struct UDPParameters UDPParameters;

typedef enum UDPQemuNumerology UDPQemuNumerology;
static udp_message_t message( UDPQemuNumerology index );
typedef int (*message_handler_t)( void*, emulator_message_t*);

struct UDPConnector {

    iUDPPort *port;
    iUDPPort* ( *init     )( struct UDPConnector* );
    int  ( *monitor       )( void* );
    void ( *destroy       )( struct UDPConnector* );
    int  ( *socket_setup  )( struct UDPConnector* );
    int  ( *receiver      )( struct UDPConnector* );
    int  ( *xmitter       )( struct UDPConnector* , void *payload, uint so_payload  );
    void ( *cleanup       )( struct UDPConnector* );
    void ( *reactor_demux )( struct UDPConnector* );

    TickObserver_t system_tick;
    UDPQemuNumerology trace_level;

    struct task_struct *monitor_task;
    kSocket *socket;
    char name[26];
    const char phy[10];
    uint32_t ip_addr;
    uint64_t mac_addr;
    struct fpga_regs *registers;

    enum {
        DefaultLocalPort = 2331,
    };
    struct msghdr replier;
    struct kvec iov;
    struct sockaddr_in *addr;
    struct sockaddr_in *reply_addr;
    uint32_t local_port;
    UDPParameters remote;
    unsigned char payload[ 128 ];
    message_handler_t reactor[32];
};

typedef struct UDPConnector UDPConnector;
static int udp_connection  ( UDPConnector *connector, emulator_message_t *m );
static int chipset_updater ( UDPConnector *connector, emulator_message_t *m );
static int QEMUTickHandler ( UDPConnector *connector, emulator_message_t *m );
static int QEMUTickPing( void *context );

static emulator_message_t endianize( uint* payload );

static iUDPPort *udp_init( UDPConnector *connector);
static int socket_setup ( UDPConnector *connector);

static void udp_destroy  ( UDPConnector* );
static int  udp_monitor  ( void *context );
static int  udp_receiver ( UDPConnector* );
static int  udp_xmitter  ( UDPConnector* , void *payload, uint so_payload );
static void udp_cleanup  ( UDPConnector *connector );
static void udp_demux    ( UDPConnector *connector );

static uint32_t  IPAddressOfPhy( const char *devname );
static uint64_t MacAddressOfPhy( const char *devname );

static struct UDPConnector connector =
{
        .init        = udp_init,
        .destroy     = udp_destroy,
        .monitor     = udp_monitor,
        .socket_setup = socket_setup,
        .receiver    = udp_receiver,
        .xmitter     = udp_xmitter,
        .cleanup     = udp_cleanup,
        .reactor_demux = udp_demux,
        .phy = "eth0",
        .local_port = DefaultLocalPort
};

static void UDPDestroy( void ){ connector.destroy( & connector ); }
static void UDPXmitt( void *payload )
{
    static const size_t so_payload = sizeof( udp_message_t );
    udp_message_t m1 = { .header = 1 << ChipSetUpdate };
    m1.header = cpu_to_le32( m1.header );
    memcpy( & m1.chipset, payload, sizeof( m1.chipset ) );
    connector.xmitter( & connector, ( void* )( & m1 ), so_payload );
}


iUDPPort *UDPConnection(const char *name, void *chipset_image )
{
    strncpy( connector.name , name, sizeof( connector.name ));
    printk( "%s.%s\n", connector.name, __FUNCTION__  );
    connector.registers = ( struct fpga_regs* )chipset_image;
    return connector.init( & connector );
}

static iUDPPort *udp_init( struct UDPConnector *connector )
{
    connector->trace_level = Silent;
    connector->ip_addr  = IPAddressOfPhy( connector->phy );
    connector->mac_addr = MacAddressOfPhy( connector->phy );
    connector->port = ( iUDPPort* )kmalloc( sizeof( iUDPPort ), GFP_KERNEL );
    connector->port->destroy = UDPDestroy;
    connector->port->xmitt = UDPXmitt;
    {
        char name[25];
        sprintf( name, "%s.UDP", connector->name );
        connector->monitor_task = kthread_run( connector->monitor, ( void* )( connector ), name );
    }
    memset( connector->reactor, 0, sizeof( connector->reactor ));
    connector->reactor[ Connection      ] = ( message_handler_t )udp_connection;
    connector->reactor[ ChipSetUpdate   ] = ( message_handler_t )chipset_updater;
    connector->reactor[ QEMUTick        ] = ( message_handler_t )QEMUTickHandler;
    return connector->port;
}

#include <linux/inetdevice.h>
static uint32_t IPAddressOfPhy( const char *devname )
{
    uint32_t addr = 0;
    struct net_device* dev = dev_get_by_name( & init_net, devname );
    if ( dev )
        addr = inet_select_addr( dev, 0, RT_SCOPE_UNIVERSE );
    dev_put( dev );
    return addr;
}

static uint64_t MacAddressOfPhy( const char *devname )
{
    uint64_t mac_address = 0;
    struct net_device* dev = dev_get_by_name( & init_net, devname );
    if ( dev )
        memcpy( & mac_address, dev->dev_addr, 6 );
    dev_put( dev );
    return mac_address >> 16;
}


static void udp_destroy( struct UDPConnector *connector )
{
    kthread_stop( connector->monitor_task );
}

static int  udp_monitor( void *context )
{


    struct UDPConnector *connector = ( struct UDPConnector *)(context);
    if( connector->socket_setup( connector ) < 0 )
    {
        kfree( connector->port );
        return -1;
    }

    printk( "%s.%s : on going on phy %s(%d.%d.%d.%d)mac(%02llx:%02llx:%02llx:%02llx:%02llx:%02llx)(%d)\n",
           connector->name, __FUNCTION__ ,
           connector->phy,

           ( connector->ip_addr & 0xff000000 ) >> 24,
           ( connector->ip_addr & 0x00ff0000 ) >> 16,
           ( connector->ip_addr & 0x0000ff00 ) >> 8,
           ( connector->ip_addr & 0x000000ff ) >> 0,

           ( connector->mac_addr & 0xff0000000000 ) >> 40,
           ( connector->mac_addr & 0x00ff00000000 ) >> 32,
           ( connector->mac_addr & 0x0000ff000000 ) >> 24,
           ( connector->mac_addr & 0x000000ff0000 ) >> 16,
           ( connector->mac_addr & 0x00000000ff00 ) >> 8,
           ( connector->mac_addr & 0x0000000000ff ) >> 0,

           connector->local_port );

    while ( ! kthread_should_stop() )
        connector->reactor_demux( connector );

    connector->cleanup( connector );
    printk( "%s.%s : quits now\n", connector->name, __FUNCTION__ );
    return 0;
}

static void udp_cleanup( UDPConnector *connector )
{
    connector->socket->ops->release( connector->socket );
    kfree( connector->port );
}

static udp_message_t message( enum UDPQemuNumerology index )
{
    udp_message_t msg;
    msg.header = index;
    return msg;
}

static int udp_receiver( UDPConnector *connector )
{
    struct msghdr msgh;
    memset( & msgh, 0, sizeof( msgh ));
    msgh.msg_flags = MSG_WAITALL;
    msgh.msg_name = ( void* )( connector->addr );
    msgh.msg_namelen  = sizeof(struct sockaddr_in);
    iov_iter_kvec( & msgh.msg_iter, READ, & connector->iov, 1, connector->iov.iov_len );
    {
        int ret_val;
        {
            mm_segment_t oldmm = get_fs();
            set_fs(KERNEL_DS);
            ret_val = sock_recvmsg( connector->socket, & msgh, msgh.msg_flags );
            connector->replier.msg_name = ( struct sockaddr_in * )msgh.msg_name;
            set_fs(oldmm);
        }
        return ret_val;
    }
}

static int udp_xmitter( UDPConnector *connector, void *payload, uint so_payload  )
{
  mm_segment_t oldmm;
  struct kvec vec;
  vec.iov_len = so_payload;
  vec.iov_base = payload;
  oldmm = get_fs(); set_fs(KERNEL_DS);
  int ret_val =  kernel_sendmsg( connector->socket, & connector->replier, &vec, 1, vec.iov_len );
  set_fs(oldmm);
  return ret_val;
}

static int socket_setup( UDPConnector *connector )
{
    connector->iov.iov_base = ( void* )( connector->payload );
    connector->iov.iov_len = sizeof( connector->payload );

    {
        static struct sockaddr_in addr;
        memset( & addr, 0, sizeof( addr ) );
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl( INADDR_ANY );
        addr.sin_port = htons(connector->local_port);
        connector->addr = & addr;
    }

    {
        int err = sock_create( AF_INET, SOCK_DGRAM, IPPROTO_UDP, & connector->socket );
        if( err < 0 )
            printk("%s: datagram socket create failured(%d)\n", connector->name, err );
        else
        {
            struct timeval tv = {0,100000};
            int flag = 1;
            mm_segment_t fs = get_fs();
            set_fs(KERNEL_DS);
            kernel_setsockopt(connector->socket, SOL_SOCKET, SO_RCVTIMEO , (char * )&tv, sizeof(tv));
            kernel_setsockopt(connector->socket, SOL_SOCKET, SO_REUSEADDR , (char * )&flag, sizeof(int));
            kernel_setsockopt(connector->socket, SOL_SOCKET, SO_REUSEPORT , (char * )&flag, sizeof(int));
            set_fs(fs);

            err = connector->socket->ops->bind( connector->socket, ( struct sockaddr *)( connector->addr ), sizeof( struct sockaddr ) );
            if( err < 0 )
                printk( KERN_INFO "%s: binding error(%d)\n", connector->name, -err );
        }

        connector->replier.msg_name = connector->addr;
        connector->replier.msg_namelen =  sizeof(struct sockaddr_in);
        connector->replier.msg_control = NULL;
        connector->replier.msg_controllen = 0;
        connector->replier.msg_flags = 0;

        return err < 0 ? err : 0;
    }

}

/* The UDP Reactor Kernel style */

static void udp_demux( UDPConnector *connector )
{
    int so_payload = 0;
    const int so_expected_payload = sizeof( udp_message_t );
    if( ( so_payload = connector->receiver( connector ) ) < 0 )
        return;

    if(  so_payload >= so_expected_payload )
    {
        if( connector->trace_level > Verbose )
            printk( "%s.%s: receives (%d bytes)\n", connector->name, __FUNCTION__ , so_payload );

        if( so_payload == so_expected_payload )
        {
            emulator_message_t m = endianize( ( uint* )connector->payload );
            uint i;
            for( i = 0; m.header; i++ )
            {
                if( connector->reactor[ i ] && test_bit( i, KERNEL_REF(m.header) ) )
                {
                    if( connector->reactor[i]( connector, & m ) == 0 )
                    {
                        connector->xmitter( connector, ( struct emulator_message_t* )connector->payload, so_payload );
                        printk( "%s.%s : echoes operation(%2d)\n", connector->name, __FUNCTION__ , i );
                    }
                }
                clear_bit( i, KERNEL_REF(m.header) );
            }
        }
    }

}

static emulator_message_t endianize( uint* payload )
{
    emulator_message_t m;
    uint i, *p = ( uint* )( & m );
    for( i = 0; i < ( sizeof( emulator_message_t ) >> 2 ); i++ )
        p[i] = le32_to_cpu( payload[ i ] );
    return m;
}

static int udp_connection( UDPConnector *connector, emulator_message_t *m )
{
    printk( "%s.%s : replied\n", connector->name, __FUNCTION__ );
    connector->port->xmitt( connector->registers );
    return 0;
}

static int chipset_updater( UDPConnector *connector, emulator_message_t *m )
{
#define UpdateCommand(m) ((uint32_t*)m->payload)[0]
#define UpdateParameter(m) ((uint32_t*)m->payload)[1]
    uint32_t command = UpdateCommand(m);

    sync_chipset_image( command, UpdateParameter(m) );
    if( connector->trace_level > Silent )
        printk( "%s.%s : processing (m%08x:$%08x)\n",
                connector->name, __FUNCTION__ , command, UpdateParameter(m) );

    return -1;
}

static int QEMUTickHandler( UDPConnector *connector, emulator_message_t *m )
{
#define HeartBeatCommand(m) ((uint32_t*)m->payload)[0]
#define HeartBeatParameter(m) ((uint32_t*)m->payload)[1]
#define MinimumTick 50

    uint32_t command  = HeartBeatCommand(m);
    uint32_t tick_val = HeartBeatParameter(m);

    printk( "%s.%s : processing $%08x(tick-feedback %s)(%d msec)\n",
            connector->name, __FUNCTION__ , command,
            command == QEMUTickEnable ? "requested" : command == QEMUTickDisable ? "disabled" : "unknown operator",
            tick_val );
    {
        TickObserver_t *tick = & connector->system_tick;
        if( command == QEMUTickEnable )
        {
            if( tick_val < MinimumTick )
            {
                printk("WARNING: Tick activity ignored : %d exceeds minimum value of %d\n", tick_val, MinimumTick );
                return -1;
            }
            {
                tick->feedback.symbol  = QEMUTickPing;
                tick->feedback.context = (void*)connector;
                tick->interval = tick_val;
                tick->deadline = 5000;
                tick->elapsed = 0;
                tick->jiffies = jiffies;
            }
            watchdog_startup( ( const TickObserver_t* )( & connector->system_tick ) );

        }
        else if( command == QEMUTickDisable )
            tick->deadline = 0;
    }
    return -1;

}




static int QEMUTickPing( void *context )
{
    UDPConnector *connector = ( UDPConnector *)context;
    TickObserver_t *tick = & connector->system_tick;
    if( tick->deadline == 0 )
        return -2;
    {
        udp_message_t m;
        m.header= le32_to_cpu( 1 << QEMUTick );

        {
            unsigned long interval = jiffies_to_msecs( jiffies - tick->jiffies );
            m.chipset.watchdog_ctrl= le32_to_cpu( interval );
            tick->elapsed += interval;
            connector->xmitter(connector, ( void* )( & m ), sizeof( m ) );

            tick->jiffies = jiffies;
            if( !( tick->elapsed % 15000 ) )
                printk( "%s.%s(%d) :\n", connector->name, __FUNCTION__ , tick->elapsed );
        }
    }
    return -1;
}
