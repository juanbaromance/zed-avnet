#ifndef QemuFPGAiface
#define QemuFPGAiface

#include "fpga_drv.h"

enum UDPQemuNumerology
{
    ShutDown = 31,
    Probe = 30,
    Connection = 29,
    ChipSetUpdate = 28,
    QEMUTick = 27,

    QEMUTickEnable  = 0xaa55ffee,
    QEMUTickDisable = ~QEMUTickEnable,
    QEMUGPiUpdate    = 31,

    Silent = 0,
    Verbose = 1,
    MoreVerbose = 2,
    Debug = 3
};

typedef struct udp_message_t {
    unsigned int header;
    struct fpga_regs chipset;
} udp_message_t;


typedef struct emulator_message_t
{
    unsigned int  header;
    unsigned char payload[60];
}emulator_message_t;

#endif
