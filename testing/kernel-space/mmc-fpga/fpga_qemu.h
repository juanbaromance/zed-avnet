#ifndef FPGAQEMUinc
#define FPGAQEMUinc

#include <linux/types.h>
#include "QemuFPGA.iface.h"
#define KERNEL_REF(a) ((volatile unsigned long *)(& a))

void QemuFPGAStartUp ( void *fpga_vm );
void QemuFPGAFinish  ( void );

typedef int (*observer_symbol_t)( void *context );
typedef struct GenericObserver_t
{
    observer_symbol_t symbol;
    void *context;
}GenericObserver_t;

typedef struct TickObserver_t
{
    GenericObserver_t feedback;
    size_t interval, deadline, elapsed;
    unsigned long jiffies;

}TickObserver_t;

void watchdog_startup( const TickObserver_t* );
void sync_chipset_image( uint32_t operation, uint32_t reg_val );

#endif
