#include "fpga_drv.h"
#include "fpga_qemu.h"

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/fcntl.h>
#include <linux/proc_fs.h>
#include <linux/spinlock_types.h>
#include <linux/sysctl.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/ioport.h>
#include <linux/platform_device.h>
#include <linux/irq.h>
#include <linux/of_address.h>

// #include <mpc512x.h>

#ifndef resource_size
#define resource_size(res) ((res)->end - (res)->start + 1)
#endif



static int procfile_read(char *buf, char **start, off_t off, int count, int *eof, void *data);
static const char* device_name = "FPGA.a3633";
static const char* fpga_fd="fpga_fd";

typedef struct
{
	int size;
	unsigned int fpga_base;
	volatile __u8  *fpga_addr;
} fpga_dev_t;

static spinlock_t lock;
static fpga_dev_t device;

#define WRITE_BYTE_FPGA(addr, value) \
		write_fpga_reg(addr, 1, value)

#define WRITE_WORD_FPGA(addr, value) \
		write_fpga_reg(addr, 2, value)

#define WRITE_DWORD_FPGA(addr, value) \
		write_fpga_reg(addr, 4, value)

static inline void write_fpga_reg( __u8 *addr, int size, int value)
{
	int i;
	for(i = 0; i < size; i++)
		out_8(addr + i, (value & (0xFF << (i << 3))) >> (i << 3));
}

#define READ_BYTE_FPGA(addr) \
		read_fpga_reg(addr, 1)

#define READ_WORD_FPGA(addr) \
		read_fpga_reg(addr, 2)

#define READ_DWORD_FPGA(addr) \
		read_fpga_reg(addr, 4)

static inline int read_fpga_reg(__u8 *addr, int size)
{
	int i;
	int value = 0;

	for(i = 0; i < size; i++)
		value |= (in_8(addr + i) << (i << 3));

	return value;
}

static inline void write_fpga_color(struct fpga_color_regs *regs, int freq_div, int duty_div)
{
	__u8 *aux = NULL;
	unsigned long flags;

	spin_lock_irqsave(&lock, flags);

	aux =  (__u8*)&(regs->control);
	WRITE_BYTE_FPGA(aux, FPGA_CONTROL_RESET_BIT);

	aux =  (__u8*)&(regs->freq_div);
	WRITE_DWORD_FPGA(aux, freq_div);

	aux =  (__u8*)&(regs->duty_div);
	WRITE_WORD_FPGA(aux, duty_div);

	aux =  (__u8*)&(regs->control);
	WRITE_BYTE_FPGA(aux, (*aux | FPGA_CONTROL_DUTY_BIT | FPGA_CONTROL_FREQ_BIT));
	WRITE_BYTE_FPGA(aux, (*aux & ~(FPGA_CONTROL_DUTY_BIT | FPGA_CONTROL_FREQ_BIT | FPGA_CONTROL_RESET_BIT)));

	spin_unlock_irqrestore(&lock, flags);
}

static inline void read_fpga_color(struct fpga_color_regs *regs, int *control, int *freq_div, int *duty_div)
{
	__u8 *aux = NULL;
	unsigned long flags;

	spin_lock_irqsave(&lock, flags);

	aux =  (__u8*)(&(regs->control));
	*control = READ_BYTE_FPGA(aux);

	aux =  (__u8*)(&(regs->freq_div));
	*freq_div = READ_DWORD_FPGA(aux);

	aux =  (__u8*)(&(regs->duty_div));
	*duty_div = READ_WORD_FPGA(aux);

	spin_unlock_irqrestore(&lock, flags);
}

static inline void read_fpga_version(struct fpga_version_regs *regs, int *code, int *ver, char *rev, int *firm, int *dna_h, int *dna_l)
{
	__u8 *aux = NULL;
	unsigned long flags;

	spin_lock_irqsave(&lock, flags);

	aux =  (__u8*)(&(regs->code));
	*code = READ_WORD_FPGA(aux);

	aux =  (__u8*)(&(regs->ver));
	*ver = READ_BYTE_FPGA(aux);

	aux =  (__u8*)(&(regs->rev));
	*rev = READ_BYTE_FPGA(aux);

	aux =  (__u8*)(&(regs->firmware));
	*firm = READ_WORD_FPGA(aux);

	aux =  (__u8*)(&(regs->dna));
	*dna_l = READ_DWORD_FPGA(aux);
	*dna_h = READ_DWORD_FPGA(aux + 4);

	spin_unlock_irqrestore(&lock, flags);
}

static ssize_t dev_write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
	int err = 0;
	fpga_gate_t gate;
	unsigned long flags;
	volatile struct fpga_regs *regs = (struct fpga_regs *)(device.fpga_addr);

	if (count != sizeof(gate))
	{
		printk(KERN_INFO "FPGA driver: Wrong size of data %d vs %d\n", (int) count, (int) sizeof(fpga_gate_t));
		return -EINVAL;
	}

	if (copy_from_user(&gate, buf, sizeof(gate)))
		return -EACCES;

	switch (gate.cmd)
	{
		case FPGA_WRITE_RGB:
		{
#if 0
			printk(KERN_INFO "Gate values:\n"
					" r.freq_div = %08X r.duty_div = %02X\n"
					" g.freq_div = %08X g.duty_div = %02X\n"
					" b.freq_div = %08X b.duty_div = %02X\n",
					gate.r.freq_div, gate.r.duty_div,
					gate.g.freq_div, gate.g.duty_div,
					gate.b.freq_div, gate.b.duty_div);
#endif

			write_fpga_color((struct fpga_color_regs *)&(regs->r), gate.r.freq_div, gate.r.duty_div);
			write_fpga_color((struct fpga_color_regs *)&(regs->g), gate.g.freq_div, gate.g.duty_div);
			write_fpga_color((struct fpga_color_regs *)&(regs->b), gate.b.freq_div, gate.b.duty_div);

			break;
		}
		case FPGA_WRITE_BLINK:
			write_fpga_color((struct fpga_color_regs *)&(regs->blink), gate.blink.freq_div, gate.blink.duty_div);
			break;

		case FPGA_READ_VERSION:
			read_fpga_version((struct fpga_version_regs *)&(regs->version), &(gate.version.code), &(gate.version.ver), &(gate.version.rev), &(gate.version.firm), &(gate.version.dna_h), &(gate.version.dna_l));

			if (copy_to_user(buf, &gate, sizeof(gate)))
				err = -EACCES;
			break;

		case FPGA_WRITE_REG:
			switch(gate.size)
			{
				case 1:
					spin_lock_irqsave(&lock, flags);
					WRITE_BYTE_FPGA((__u8*)(device.fpga_addr + gate.offset), gate.value);
					spin_unlock_irqrestore(&lock, flags);
					break;
				case 2:
					spin_lock_irqsave(&lock, flags);
					WRITE_WORD_FPGA((__u8*)(device.fpga_addr + gate.offset), gate.value);
					spin_unlock_irqrestore(&lock, flags);
					break;
				case 4:
					spin_lock_irqsave(&lock, flags);
					WRITE_DWORD_FPGA((__u8*)(device.fpga_addr + gate.offset), gate.value);
					spin_unlock_irqrestore(&lock, flags);
					break;
				default:
					printk(KERN_INFO "FPGA driver: Wrong size of data. Valid size [1, 2, 4]\n");
					err = -EINVAL;
					break;
			}
			break;
		case FPGA_READ_REG:
			switch(gate.size)
			{
				case 1:
					spin_lock_irqsave(&lock, flags);
					gate.value = READ_BYTE_FPGA((__u8*)(device.fpga_addr + gate.offset));
					spin_unlock_irqrestore(&lock, flags);
					break;
				case 2:
					spin_lock_irqsave(&lock, flags);
					gate.value = READ_WORD_FPGA((__u8*)(device.fpga_addr + gate.offset));
					spin_unlock_irqrestore(&lock, flags);
					break;
				case 4:
					spin_lock_irqsave(&lock, flags);
					gate.value = READ_DWORD_FPGA((__u8*)(device.fpga_addr + gate.offset));
					spin_unlock_irqrestore(&lock, flags);
					break;
				default:
					printk(KERN_INFO "FPGA driver: Wrong size of data. Valid size [1, 2, 4]\n");
					err = -EINVAL;
					break;
			}

			if(!err)
			{
				if (copy_to_user(buf, &gate, sizeof(gate)))
					err = -EACCES;
			}
			break;
		default:
			printk(KERN_INFO "FPGA driver: Invalid command[%d]\n", gate.cmd);
			err = -EINVAL;
			break;
	}

	return err;
}

static ssize_t dev_read(struct file *file, char *buf, size_t count, loff_t *ppos){ return -EACCES; }
static int dev_open(struct inode *inode, struct file *file){ return 0 ;}
static int dev_release(struct inode *inode, struct file *file){	return 0; }

static struct file_operations fops = {
    owner: THIS_MODULE,
    write: dev_write,
    read: dev_read,
    open: dev_open,
    release: dev_release
};


static int procfile_read(char *buf, char **start, off_t off, int count, int *eof, void *data)
{
    unsigned long flags;
	char *tmp = buf;
	int freq = 0, duty = 0, control = 0;
	volatile struct fpga_regs *regs = (struct fpga_regs *)(device.fpga_addr);

	tmp += sprintf(tmp, "FPGA registers:\n");
	read_fpga_color((struct fpga_color_regs *)&(regs->r), &control, &freq, &duty);
	tmp += sprintf(tmp, " r.control = %02X r.freq_div = %08X r.duty_div = %02X\n", control, freq, duty);
	read_fpga_color((struct fpga_color_regs *)&(regs->g), &control, &freq, &duty);
	tmp += sprintf(tmp, " g.control = %02X g.freq_div = %08X g.duty_div = %02X\n", control, freq, duty);
	read_fpga_color((struct fpga_color_regs *)&(regs->b), &control, &freq, &duty);
	tmp += sprintf(tmp, " b.control = %02X b.freq_div = %08X b.duty_div = %02X\n", control, freq, duty);
	read_fpga_color((struct fpga_color_regs *)&(regs->blink), &control, &freq, &duty);
	tmp += sprintf(tmp, " bl.control = %02X bl.freq_div = %08X bl.duty_div = %02X\n", control, freq, duty);

    spin_lock_irqsave(&lock, flags);
    tmp += sprintf(tmp, " beep.control = %02X\n",  READ_BYTE_FPGA((__u8*)(&(regs->beep_ctrl))));
    tmp += sprintf(tmp, " watchdog.control = %08X\n", READ_DWORD_FPGA((__u8*)(&(regs->watchdog_ctrl))));
    tmp += sprintf(tmp, " heartbeat.control = %08X\n", READ_DWORD_FPGA( (__u8*)(&(regs->heartbeat_ctrl))));
	spin_unlock_irqrestore(&lock, flags);

	return tmp - buf;
}

static char *fpga_get_version(void)
{
#ifdef fixme
	extern const char* fpga_drv_svninfo;
	sprintf(str, "Driver:\n"		" Created from [%s]\n", fpga_drv_svninfo );
#endif
	return "A3633.fpga-driver" ;
}

#define FPGAMagicMapping (( __u8* )0xdeadbeef)
static int driver_init_module(void)
{
   	
	printk( KERN_INFO "%s\n", fpga_get_version() );
	spin_lock_init( & lock );
	device.fpga_addr = FPGAMagicMapping;

	do
	{
		struct device_node *np = NULL;
		{
        	const char *dts_entry = true ? "VGA" : "fsl,mpc5121ads-cpld";
			if (!(np = of_find_compatible_node(NULL, NULL, dts_entry)))
			{
				printk(KERN_WARNING "%s.%s: entry \"%s\" not found on current dts\n",
					   device_name, __FUNCTION__, dts_entry);
				break;
			}
		}

		{
			struct resource res;
            if ( of_address_to_resource(np, 0, &res))
				break;

			device.fpga_base = res.start;
			device.size = resource_size(&res);
		}

		{
			if ( ! request_mem_region(device.fpga_base, device.size, "A3633.fpga") )
				break;

			device.fpga_addr = ioremap_nocache(device.fpga_base, device.size);
			if (!device.fpga_addr)
			{
				printk(KERN_WARNING "%s.%s: iomem $%08x virtual remapping : f a i l u r e\n",
					   device_name, __FUNCTION__, device.fpga_base);
				release_mem_region(device.fpga_base, device.size);
				break;
			}

			printk(KERN_INFO "%s(%s): %d%s bytes mapped from phy($%08x) to virt($%p)\n",
				   device_name, np->name,
				   device.size > 1024 ? device.size >> 20 : device.size, 
				   device.size > 1024 ? "M" : "", 
				   device.fpga_base, device.fpga_addr);
		}

        proc_create( fpga_fd,0666,NULL, & fops );

#ifndef A3633Hardware
		{
			QemuFPGAStartUp( ( void* )device.fpga_addr );

			fpga_gate_t gate;
			volatile struct fpga_regs *regs = ( struct fpga_regs * )( device.fpga_addr );
			read_fpga_version( (struct fpga_version_regs *)&(regs->version), 
					&(gate.version.code), &(gate.version.ver), &(gate.version.rev), &(gate.version.firm), &(gate.version.dna_h), &(gate.version.dna_l));
            printk( KERN_INFO "A%d-%02d-%c: DNA ( %08x%08x ):Firmware( $%04x )\n",
                    gate.version.code, gate.version.ver, gate.version.rev,
                    gate.version.dna_h, gate.version.dna_l, gate.version.firm );
	
		}
#endif

#ifdef fixme
		printk(KERN_INFO "FPGA driver: Registering driver in /proc/fpga_fd and /proc/fpga_sts\n");
		create_proc_read_entry("fpga_sts", 0, NULL, procfile_read, NULL);
#endif
		break;

	} while (1);

	return 0;
}

static void driver_exit_module(void)
{
	printk( KERN_INFO "%s.%s : Resources releasing\n" , device_name, __FUNCTION__ );
	if (device.fpga_addr == FPGAMagicMapping )
		return;

#ifndef A3633Hardware	
	QemuFPGAFinish();
#endif

	iounmap(device.fpga_addr);
	release_mem_region(device.fpga_base, device.size);
    remove_proc_entry( fpga_fd, NULL );

#ifdef __fixme
	remove_proc_entry("fpga_sts", NULL);
#endif

	printk( KERN_INFO "%s.%s:  Bye!\n" , device_name , __FUNCTION__ );
}


module_init( driver_init_module);
module_exit( driver_exit_module);

MODULE_DESCRIPTION("FPGA mp5121-a3633/QEmu module");
MODULE_AUTHOR("Joaquin Suarez/Juanba");
MODULE_LICENSE("GPL");
