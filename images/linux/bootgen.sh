#!/bin/bash
bif_spec=$1
boot_spec=$2


if [ -z "${bif_spec}" ]; then 
bif_spec="system.bif"
fi

if [ -z "${boot_spec}" ]; then 
boot_spec="BOOT.BIN"
fi

if [ -z "${PETALINUX}" ]; then
source ~/zynq-development/petalinux-sdk/2019.2/settings.sh
fi

C4='\033[00;34m'
C5='\033[00;32m'
B='\033[00;1m'
RC='\033[0m'

printf "\n"

date
printf "Generating ZynQ ROM image ${C5}${boot_spec}${RC} using ${C4}${bif_spec}${RC} specification values :: \n\n`cat ${bif_spec}`\n"
$PETALINUX/tools/xsct/bin/bootgen -image ${bif_spec} -r -w -o i ${boot_spec} -arch zynqmp
[ "$?" = "0" ]&& ls -al -H ${boot_spec};