$PETALINUXHOME/tools/linux-i386/petalinux/bin/qemu-system-aarch64 \
-m 2048 -M arm-generic-fdt -nographic \
-serial mon:stdio -serial /dev/null \

-device loader,file=zynqmp_fsbl.elf,cpu=0 \ 
-hw-dtb ./zynqmp-qemu-arm.dtb \
-device loader,addr=0xfd1a0104,data=0x8000000e,data-len=4 \
-drive file=qemu_sd.img,if=sd,format=raw,index=1 \

-boot mode=5