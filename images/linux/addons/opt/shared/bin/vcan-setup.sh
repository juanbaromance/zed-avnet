# !/bin/bash

device="vcan0"
printf "# `date` : Let's build socketCAN( ${device} )\n"

if [ -z "`lsmod | grep vcan`" ]; then
  printf "# Installing socketCAN VirtualCAN driver\n"
  insmod vcan.ko;
fi;

if [ -z "`cat /proc/net/dev | grep ${device}`" ]; then
  printf "# Creating VirtualCAN device : ${device}\n"
  ip link add dev ${device} type vcan
  ip -h -d -c link show vcan0
fi;

if [ -n "`ip -h -d -c link show vcan0 | grep DOWN`" ]; then
  printf "# Bringing Up SocketCAN : ${device} link\n"
  ip link set up ${device}
else
  printf "# Network link( ${device} ) is already up\n"
fi;

ip -h -d -c link show ${device}