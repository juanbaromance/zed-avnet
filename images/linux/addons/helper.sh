#!/bin/bash

mount -n -t devtmpfs devtmpfs /dev
mount -n -t proc     proc     /proc
mount -n -t sysfs    sysfs    /sys
mount -n -t tmpfs    tmpfs    /run

export PATH=$PATH:/opt/shared/bin/:/opt/nfs/bin/

mount -o port=2049,nolock,noac,proto=tcp -t nfs 10.0.2.16:/opt/shared/ /opt/nfs
ln -nsf /opt/shared/config /opt/config

