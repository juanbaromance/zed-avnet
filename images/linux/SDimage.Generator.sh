#!/bin/bash
FSBL="zynq_fsbl.elf"
PLBitStream="system.bit"
QEMU_IMAGE="qemu_sd.img"
KERNEL="zImage"
KERNEL_DTB="system.dtb"

export LANG=""
printf "`date` Generating BOOT.BIN -> FSBL( ${FSBL} ) + PLBitStream( ${PLBitStream} ) \n\n"
petalinux-package --boot --fsbl ${FSBL} --fpga ${PLBitStream} --u-boot --force

printf "Setting Up QEmuSD vfat.image ${QEMU_IMAGE}\n\n"
dd if=/dev/zero of=${QEMU_IMAGE} bs=512M count=1

mkfs.ext4 ${QEMU_IMAGE}
sudo mount -o loop qemu_sd.img tmp/
printf "Transfering artefacts to ${QEMU_IMAGE}\n\n"
sudo cp BOOT.BIN ${KERNEL} ${KERNEL_DTB} tmp/
cd tmp;
sudo tar xvzf ../rootfs.tar.gz
sudo cp -dpR ../addons/* .
cd ..
sudo umount tmp

# mkfs.vfat -F 32 ${QEMU_IMAGE}
# mcopy -i ${QEMU_IMAGE} BOOT.BIN ::/
# mcopy -i ${QEMU_IMAGE} ${KERNEL} ::/
# mcopy -i ${QEMU_IMAGE} ${KERNEL_DTB} ::/
